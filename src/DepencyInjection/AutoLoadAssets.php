<?php

namespace src\DepencyInjection;

/**
 * Class d'include d'assets css et JS
 */

class AutoloadAssets
{
    public $assets;

    public function __construct()
    {
        // Tether
        $this->assets = "\r\n";
        $this->assets .= '<script type="text/javascript" src="/bower_components/tether/dist/js/tether.min.js"></script>';

        // jQuery
        $this->assets .= "\r\n";
        $this->assets .= '<script type="text/javascript" src="/bower_components/jquery/dist/jquery.min.js"></script>';
        $this->assets .= "\r\n";
        $this->assets .= '<script type="text/javascript" src="/bower_components/jquery-ui/jquery-ui.min.js"></script>';

        // Font awesome
        $this->assets .= "\r\n";
        $this->assets .= '<link rel="stylesheet" type="text/css" href="/bower_components/font-awesome/css/font-awesome.min.css" />';

        // Bootstrap
        $this->assets .= "\r\n";
        $this->assets .= '<script type="text/javascript" src="/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>';
        $this->assets .= "\r\n";
        $this->assets .= '<link rel="stylesheet" type="text/css" href="/bower_components/bootstrap/dist/css/bootstrap.min.css" />';

        // AngularJs
        $this->assets .= "\r\n";
        $this->assets .= '<script type="text/javascript" src="/node_modules/angular/angular.min.js"></script>';

        //Directives AngularJs
        $this->assets .= "\r\n";
        $this->assets .= '<script type="text/javascript" src="/js/angularJs/directives/bindHtmlCompile.js"></script>';
    }
}
