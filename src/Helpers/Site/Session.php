<?php 

namespace src\Helpers\Site;

use \src\Helpers\AppGlobal as app;

class Session{

	/**
     * Initialisation de la session utilisateur
     *
     * @param array | string $details La valeur de la session
     */
    static function setSession($name, $details)
    {
        $app = app::$params;
        $app['session']->set($name, $details);
    }

    /**
     * Fonction d'enregistrement de l'IP visiteur en session
     * 
     * @param string $ip L'IP du visiteur
     */
    static function SetIpOnSession(string $ip){

        $visitorSession = self::GetSession('visitor');
        $visitorSession['ip'] = $ip;
        self::setSession('visitor', $visitorSession);

    }  

    /**
     * Fonction d'initialisation de la session visitor
     */
    static function SetSessionVisitor(){

        if(self::GetSession('visitor') == 0 ){
            $details =  array('ip' => '');
            self::setSession('visitor', $details);    
        }
    }

    /**
     * Récupération d'une session
     *
     * @param string $name Le nom de la session
     */
    static function GetSession($name)
    {
        $app = app::$params;

        if(app::$params['session']->get($name) !== null ){
            return app::$params['session']->get($name);
        }else{
            return 0;
        }    
    }

    /**
     * Destruction de la session
     */
    static function destroySession($nameSession)
    {
        $app = app::$params;
        
        $app['session']->remove($nameSession);
    }

}