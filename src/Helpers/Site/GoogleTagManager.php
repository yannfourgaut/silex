<?php 

namespace src\Helpers\Site;

use src\Helpers\Site\Cookies as CookiesHelper;

/**
 * Class de Tracking Google tag manager / Google Analytics
 *
 * @author Yann Fourgaut <contact@yannfourgaut.com>
 */

class GoogleTagManager{

	/**
	 * @var string $idGoogleTagManager L'ID de suivi transmis par Google tag manager
	 */
	public $idGoogleTagManager = '';

	/**
	 * @var string $idSuiviGoogleAnalytics L'ID de suivi transmis par Google Analytics
	 */
	public $idSuiviGoogleAnalytics = ''; 

	/**
	 * @var string $script La balise HTML Google Tag Manager
	 */
	public $script;

	/**
	 * @var string $noScript L'iframe Google Tag Manager en cas de javascript désactivé
	 */
	public $noScript;	

	/**
	 * @var string $googleAnalytics La balise Google Analytics
	 */
	public $googleAnalytics;

	/**
	 * @var integer $USER_ID L'identifiant utilisateur à transmettre au suivi par ID Google Analytics
	 * 
	 * @see https://support.google.com/analytics/answer/3123666#configuration
	 */
	public $USER_ID;
	
	/**
	 * Fonction __construct
	 * 
	 * Construction des balises Google tag Manager
	 */
	public function __construct(){

		$this->script = 	
			'

			<script>
				(function(w,d,s,l,i){
					w[l]=w[l]||[];
					w[l].push({
						\'gtm.start\':
							new Date().getTime(),
							event:\'gtm.js\'
					});
					var f=d.getElementsByTagName(s)[0],
					j=d.createElement(s),
					dl=l!=\'dataLayer\'?\'&l=\'+l:\'\';
					j.async=true;
					j.src=\'https://www.googletagmanager.com/gtm.js?id=\'+i+dl;
					f.parentNode.insertBefore(j,f);
				})
				(window,document,\'script\',\'dataLayer\',\''.$this->idGoogleTagManager.'\');
			</script>
		';

		$this->noScript = "
			<!-- Google Tag Manager (noscript) -->
				<noscript>
					<iframe src='https://www.googletagmanager.com/ns.html?id={$this->idGoogleTagManager}'
					height='0' width='0' style='display:none;visibility:hidden'></iframe>
				</noscript>
			<!-- End Google Tag Manager (noscript) -->
		";

	}	

	/**
	 * getTags
	 * 
	 * @return array les balises d'inclusion de Google tag manager
	 */
	
	public function getTags(){

		return array('script' => $this->script, 'noscript' => $this->noScript);

	}

	/**
	 * Fonction de définition de l'user id à tracker selon les cookies
	 *
	 * Permets de retrouver l'utilisateur et son compte
	 * 
	 * @param  \Symfony\Component\HttpFoundation\ParameterBag $cookies L'objet HttpFoundation contenant les cookies
	 * 
	 * @return string $googleAnalytics La balise Google Analytics
	 */
	public function trackingUser( \Symfony\Component\HttpFoundation\ParameterBag $cookies ){
				
		$cookieHelper = new CookiesHelper;
		$hisCookie = $cookieHelper->getCookie($cookies, COOKIE_USER_NAME);

		// Un cookie est présent
		if( $hisCookie !== 0){
			$whoIs = $cookieHelper->FindUserByCookie($hisCookie);
			$this->USER_ID = $whoIs;
		}

		$this->googleAnalytics = "
			<script>
			  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

			  ga('create', '$this->idSuiviGoogleAnalytics', 'auto');
			  ga('set', 'userId', '{$this->USER_ID}');
			  ga('send', 'pageview');

			</script>
		";

		return $this->googleAnalytics;
		
	}

}
