<?php 

namespace src\Helpers;

/**
 * Class de conversion de dimensions selon le DPI
 *
 * @author Yann Fourgaut <contact@yannfourgaut.com>
 */

class ConvertDimensions
{

    /**
     * @var float $widthCm La largeur en centimetres
     */
    private $widthCm;

    /**
     * @var float $heightCm La hauteur en centimetres
     */
    private $heightCm;

    /**
     * @var float $widthMm La largeur convertie en millimetres
     */
    private $widthMm;

    /**
     * @var float $heightCm La hauteur convertie en millimetres
     */
    private $heightMm;

    /**
     * @var float $widthMm La largeur convertie en pixels
     */
    private $widthPx;

    /**
     * @var float $heightCm La hauteur convertie en pixels
     */
    private $heightPx;

    /**
     * @var float $widthMm La largeur convertie en points
     */
    private $widthPt;

    /**
     * @var float $heightCm La hauteur convertie en points
     */
    private $heightPt;

    /**
     * @var array $rapportDpi La tableau contenant le rapport de calcul en 75 et 300 DPI
     */
    public $rapportDpi = array(72 => 2.8346, 300 => 11.8118);

    /**
     * @var array $values Les valeurs retournées
     */
    public $values = array();

    /**
     * Fonction de construction
     *
     * Cette fonction applique toutes les conversions
     * 
     * @param float $width La largeur en centimetres
     * @param float $height La haueteur en centimetres
     * @param integer $dpi 72|300 La résolution désirée
     */
    public function __construct($width, $height, $dpi = 72)
    {
        $this->widthCm = $width;
        $this->heightCm = $height;

        $this->widthMm  = ($this->widthCm * 1000)/100;
        $this->heightMm  = ($this->heightCm * 1000)/100;


        self::calculateDpi($dpi);

        $this->values['cm'] = array();
        $this->values['cm']['width'] = $this->widthCm;
        $this->values['cm']['height'] = $this->heightCm;
        $this->values['mm'] = array();
        $this->values['mm']['width'] = $this->widthMm;
        $this->values['mm']['height'] = $this->heightMm;
        $this->values['px'] = array();
        $this->values['px']['width'] = $this->widthPx;
        $this->values['px']['height'] = $this->heightPx;
        $this->values['pt'] = array();
        $this->values['pt']['width'] = $this->widthPx*0.75;
        $this->values['pt']['height'] = $this->heightPx*0.75;
    }

    /**
     * Fonction de calcul des pixels en rapport à la résolution
     * @param float $dpi $widthPx La largeur en pixels
     * @return float $heightPx $widthPx La hauteur en pixels
     */
    private function calculateDpi($dpi)
    {
        $this->widthPx  = $this->widthMm*$this->rapportDpi[$dpi];
        $this->heightPx  = $this->heightMm*$this->rapportDpi[$dpi];
    }
}
