<?php

namespace src\Controllers\Home;

use \App\FrontController as FrontController;

/**
 * Class d'accès aux pages d'accueil
 *
 * Par convention, quand aucune action n'est appelée en query URL, c'est la class Basic qui est appelée
 * 
 * @author Yann Fourgaut <contact@yannfourgaut.com>
 */

class Basic extends FrontController
{

  public function __construct($params, $post, $get, $cookies)
  {

      $this->template = 'home.twig';
      $this->twigCalled = 1;
      
  }
}
