<?php

namespace src\Entity\Example\Example;

/**
 * Example
 */
class Example
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var ExampleType
     */
    private $enum;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set enum.
     *
     * @param ExampleType $enum
     *
     * @return Example
     */
    public function setEnum($enum)
    {
        $this->enum = $enum;

        return $this;
    }

    /**
     * Get enum.
     *
     * @return ExampleType
     */
    public function getEnum()
    {
        return $this->enum;
    }
}
