<?php 

namespace config\AllEnums;

use \YannFourgaut\EnumDoctrine\Doctrine\EnumType as EnumType;

class ExampleType extends EnumType{

	protected $name = 'ExampleType';
    public $values = array('foo', 'bar');

}