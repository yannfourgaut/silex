<?php

namespace App;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Cookie;

use Silex\Application;
use Silex\Api\ControllerProviderInterface;

use src\Controllers\Errors as ShowError;
use src\Helpers\Site\GoogleTagManager as GoogleTagManager;
use src\Helpers\AppGlobal as app;
use src\Helpers\Site\Session as SessionHelper;

use src\Helpers\DevEnvironnement\SetDevEnvironnement as SetDevEnvironnement;

/**
 * Class de rooting appelant le controlleur correspondant à la page recherchée
 *
 * @author Yann Fourgaut <contact@yannfourgaut.com>
 */

class Rooter implements ControllerProviderInterface
{
    /**
     * @var string $paramsUrl Le Query de l'url des parametres transférés derriere le second "/" ex /admin/foo=bar&bar=foo
     */
    private $paramsUrl;

    /**
     * @var string $route Le parametre saisi derriere le 1er "/" ex : /admin
     */
    private $route;

    /**
     * @var object $httpPost l'objet contenant les HTTP_POST retourné par HttpFoundation de Symfony
     */
    private $httpPost;

    /**
     * @var object $httpGet l'objet contenant les HTTP_GET retourné par HttpFoundation de Symfony
     */
    private $httpGet;

     /**
     * @var object $cookies l'objet contenant les cookies retourné par HttpFoundation de Symfony
     */
    private $cookies;

    /**
     * @var int $twigCalled Boolean Un template twig est-il appelé?
     */
    public $twigCalled = 0;

    /**
     * @var string $template Le nom du template twig appelé
     */
    public $template = '';

    /**
     * @var string $head Le contenu HTML de la balise HEAD
     */
    public $head = '';

     /**
     * @var object $response L'objet response Symfony
     */
    public $response;

    /**
     * @var array $params Les parametres transmis par le controller appelé, à transmettre à TWIG
     */
    public $params = '';

    /**
     * @var object $json Le JSON transmis par le controller appelé
     */
    public $json = '';

    /**
     * @var string $redirect Le lien de redirection transmis par le controller appelé
     */
    public $redirect;

    /**
     * @var array $GoogleTagManager Le tableau contenant les balises Google Tag Manager à transmettre à TWIG
     */
    public $GoogleTagManager;

    /**
     * @var string $visitorIp L'IP du visiteur du site
     */
    public $visitorIp;


    /**
     * Controllers collection
     *
     * Déclaration des controllers par route
     */
    public function connect(Application $app){

        $controllers = $app['controllers_factory'];

        $controllers->get('/', function (Application $app) {
            self::setRooter('home');
            return self::defineReturns();
        });

        $controllers->get('/{route}', function (Application $app, $route) use($app){
            self::setRooter($route);
            return self::defineReturns();
        });

        $controllers->post('/{route}',  function (Application $app,$route) use($app){
            self::setRooter($route);
            return self::defineReturns();
        });

        $controllers->get('/{route}/{params}', function (Application $app, $route,$params) use($app){
            self::setRooter($route, $params);
            return self::defineReturns();
        });

        $controllers->post('/{route}/{params}',  function (Application $app, $route, $params) use($app){
            self::setRooter($route, $params);
            return self::defineReturns();
        });

        $app->error(function (\Exception $e, $code) use ($app) {
            if (404 === $code) {
                return $app->redirect('/Errors/');
            }
        });

        return $controllers;    

    }

    /**
     * Fonction d'enregistrement des entêtes HTTP via le HttpFoundation de Symfony
     */
    public function setRequest(){

        $request = Request::createFromGlobals();

        $this->httpPost = $request->request;
        $this->httpGet = $request->query;
        $this->cookies = $request->cookies;
        $this->visitorIp = $request->getClientIp();
 
    }

    /**
     * Fonction d'appel logique au controller correspondant à la requête HTTP
     * @param string $route Le parametre saisi derriere le 1er "/" ex : /admin
     * @param string $paramsUrl Le Query de l'url des parametres transférés derriere le second "/" ex /admin/foo=bar&bar=foo
     * @param object $infos l'objet contenant les HTTP_POST, HTTP_GET, cookies ect... retourné par HttpFoundation de Symfony
     */
    
    public function setRooter(string $route = 'accueil', string $paramsUrl = '')
    {   
        
        self::setRequest();

        SessionHelper::SetSessionVisitor(); // création du $_SESSION['visitor'];
        SessionHelper::SetIpOnSession( $this->visitorIp ); // Enregistrement de l'IP en session "visitor"


        $this->paramsUrl = $paramsUrl;
        $this->route = $route;

        $params = array();
        if (!empty( $this->route )) {
            $returns = self::buildNamespace();
            $folder = $returns[0];
            $params = $returns[1];
        }


        // Construction de la balise HEAD avec les assets globaux
        $autoloadHead = new \src\DepencyInjection\AutoLoadAssets;
        $this->head = $autoloadHead->assets;

        // Instanciation du controller target
        $classToCall =
        ( isset($params) && !empty($params) && $params['action'] !== null && !empty($params['action']) )
        ? $request = ucwords($params['action'], "-")
        : 'Basic';
        $namespace = "\\src\\Controllers\\{$folder}\\{$classToCall}";

        
        if(class_exists($namespace)){
            $controller = new $namespace($params,$this->httpPost,$this->httpGet,$this->cookies);
        }
        else{
             $controller = new ShowError\Basic($params,$this->httpPost,$this->httpGet,$this->cookies);
        }

        /**
         * Traitement de la réponse du controller
         */
        if( isset( $controller->response ) ){
            $this->response = $controller->response;
        }

        if( isset( $controller->twigCalled ) ){
            $this->twigCalled = $controller->twigCalled;
        }

        if( isset( $controller->template ) ){

            $this->template = $controller->template;

            // Si template, c'est donc un retour html, donc construction des balises Google de tracking
            $GoogleTagManager = new GoogleTagManager($this->visitorIp);
            $this->GoogleTagManager['gtm'] = $GoogleTagManager->getTags();
            $this->GoogleTagManager['ga'] = $GoogleTagManager->trackingUser($this->cookies);
        }

        if( isset( $controller->params ) ){
            $this->params =  $controller->params;
        }

        if( isset( $controller->json ) ){
            $this->json =  $controller->json;
        }
        if(isset($controller->redirect)){
            $this->redirect = $controller->redirect;
        }
    }

    /**
     * Fonction de construction du dossier de la class à appeler dans le controller en respectant le CamelCase
     * @return array [0] Le chemin du controller, [1] Les parametres query en url transformés en tableau clé = valeur
     */
    private function buildNamespace()
    {
        $request = ucwords($this->route, "-");
        $folder = str_replace('-','',$request);

        $explode = explode('&',$this->paramsUrl);
        $requestedParams = $explode;

        $params = array();

        foreach ($requestedParams as $param) {
            $explode = explode('=', $param);
            if( isset($explode[0]) AND isset($explode[1]) ){
                $params[ $explode[0] ] = $explode[1];
            }
        }

        $folder = (string) $folder;
        return array($folder,$params);
    }

    /**
     * Gestion de l'ordre de retour
     *     - response ( set cookies)
     *     - redirect
     *     - echo twig
     *     - echo json
     */
    public function defineReturns(){

        $app = \src\Helpers\AppGlobal::$params;

        if(!empty($this->response)){
            return $this->response;
        }

        elseif(!empty($this->redirect)){
            return $app->redirect($this->redirect);
        }

        elseif($this->twigCalled === 1){


            return $app['twig']->render(
                $this->template,
                array(
                    'head' => $this->head,
                    'params' => $this->params,
                    'server' => $_SERVER,
                    'environnement' => ENVIRONNEMENT,
                    'GoogleTagManager' => $this->GoogleTagManager
                    )
                );

        }
        elseif($this->json){
            return $app->json($this->json);
        }

    }
}
